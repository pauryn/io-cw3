package cw3;

        import java.awt.event.*;
        
public class Cw3 {
    public static void main(String[] args) {
        Frame frame=new Frame();
        frame.setVisible(true);
        frame.jButton2.addActionListener((ActionEvent e) -> {
            try{
                frame.getdoubles();
                Add dodaj=new Add(frame.val1,frame.val2);
                frame.jTextField3.setText("Wynik wynosi: "+dodaj.c);
            }
            catch(NumberFormatException nfe){    
                frame.jTextField3.setText("Bledne dane wejsciowe!");
            }
        });
        
        frame.jButton1.addActionListener((ActionEvent e) -> {
            try{
                frame.getdoubles();
                Sub odejmij=new Sub(frame.val1,frame.val2);
                frame.jTextField3.setText("Wynik wynosi: "+odejmij.c);
            }
            catch(NumberFormatException nfe){    
                frame.jTextField3.setText("Bledne dane wejsciowe!");
            }
        });
        
        frame.jButton3.addActionListener((ActionEvent e) -> {
            try{
                frame.getdoubles();
                Mul pomnoz=new Mul(frame.val1,frame.val2);
                frame.jTextField3.setText("Wynik wynosi: "+pomnoz.c);
            }
            catch(NumberFormatException nfe){    
                frame.jTextField3.setText("Bledne dane wejsciowe!");
            }
        });
        
        frame.jButton4.addActionListener((ActionEvent e) -> {
            try{
                frame.getdoubles();
                if (frame.val2==0){
                    frame.jTextField3.setText("Nie dzielimy przez 0!");
                }
                else{
                    Div podziel=new Div(frame.val1,frame.val2);
                    frame.jTextField3.setText("Wynik wynosi: "+podziel.c);
                }
                
            }
            catch(NumberFormatException nfe){    
                frame.jTextField3.setText("Bledne dane wejsciowe!");
            }
        });
        
    }
}